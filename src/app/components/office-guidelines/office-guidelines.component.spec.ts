import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OfficeGuidelinesComponent } from './office-guidelines.component';

describe('OfficeGuidelinesComponent', () => {
  let component: OfficeGuidelinesComponent;
  let fixture: ComponentFixture<OfficeGuidelinesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OfficeGuidelinesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OfficeGuidelinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
