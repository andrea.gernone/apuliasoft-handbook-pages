import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComunicationChannelsComponent } from './comunication-channels.component';

describe('ComunicationChannelsComponent', () => {
  let component: ComunicationChannelsComponent;
  let fixture: ComponentFixture<ComunicationChannelsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComunicationChannelsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComunicationChannelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
