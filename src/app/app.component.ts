import { ViewportScroller } from '@angular/common';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'apuliasoft-handbook';
  constructor(private viewportScroller: ViewportScroller){}

  public onClick(elementId: string): void {
      this.viewportScroller.scrollToAnchor(elementId);
  }
}
