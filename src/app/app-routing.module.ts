import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { BestPracticeComponent } from './components/best-practice/best-practice.component';
import { CompanyCultureComponent } from './components/company-culture/company-culture.component';
import { ComunicationChannelsComponent } from './components/comunication-channels/comunication-channels.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { MainComponent } from './components/main/main.component';
import { OfficeGuidelinesComponent } from './components/office-guidelines/office-guidelines.component';
import { PoliciesComponent } from './components/policies/policies.component';

const routes: Routes = [];

@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {path: '', component: MainComponent},
      {path: 'cultura-aziendale', component: CompanyCultureComponent},
      {path: 'dashboard', component: DashboardComponent},
      {path: 'policies-aziendali', component: PoliciesComponent},
      {path: 'office-guidelines', component: OfficeGuidelinesComponent},
      {path: 'canali-di-comunicazione', component: ComunicationChannelsComponent},
      {path: 'best-practice-operative', component: BestPracticeComponent}
    ]),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
