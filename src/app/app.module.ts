import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './components/main/main.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { CompanyCultureComponent } from './components/company-culture/company-culture.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { PoliciesComponent } from './components/policies/policies.component';
import { OfficeGuidelinesComponent } from './components/office-guidelines/office-guidelines.component';
import { ComunicationChannelsComponent } from './components/comunication-channels/comunication-channels.component';
import { BestPracticeComponent } from './components/best-practice/best-practice.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    SidebarComponent,
    CompanyCultureComponent,
    DashboardComponent,
    PoliciesComponent,
    OfficeGuidelinesComponent,
    ComunicationChannelsComponent,
    BestPracticeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
